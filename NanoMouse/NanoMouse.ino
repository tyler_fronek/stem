#include <Servo.h>

Servo leftServo;
Servo rightServo;

const byte ledPin = 13;
const byte buttonPin = 9;

const byte power = 250;

void forward()
{
leftServo.writeMicroseconds(1500-power);
 rightServo.writeMicroseconds(1500+power);
}

void stop()
{
  leftServo.writeMicroseconds(1500);
 rightServo.writeMicroseconds(1500);
}
void setup()
  
{
  leftServo.attach(6);
  rightServo.attach(5);
    pinMode(ledPin,OUTPUT);
    pinMode(buttonPin,INPUT_PULLUP); 

   while(digitalRead(buttonPin))
   {}
   forward();
   delay(1800);
   stop();
}

void loop()
{
digitalWrite(ledPin,HIGH);
  
}